ARG ARCH=amd64
ARG ALPINE_VERSION=3.10
FROM $ARCH/alpine:$ALPINE_VERSION
RUN apk add --no-cache docker-cli
ENTRYPOINT ["docker"]
